var http = require('http');
var _ = require('underscore');
var request;

var reqHeaders = {
    hostname:'platform.bluemessaging.net',
    method:'POST',
    path:'/services/bm-api?wsdl',
    headers: {
        'Content-Type':'text/xml;charset=UTF-8'
    }
};

var authData = {
    bmToken   : process.env.BMTOKEN,
    bmDomain  : process.env.BMDOMAIN,
    bmTemplate: process.env.BMTEMPLATE
};

function makeRequest(fillData, callback) {
    var bodyReq = integXml(templHelper(fillData));
    //console.log(bodyReq);
    reqHeaders.headers['Content-Length'] = bodyReq.length;

    request=http.request(reqHeaders, callback);

    request.on('error', function(err) {
        console.error(err);
    });

    request.write(bodyReq);
    request.end();
}

// Functions which will be available to external callers
exports.makeRequest = makeRequest;

function templHelper(fillData) {
    /* parse only once keys from object to ensure same order */
    var keys = _.keys(fillData);
    return _.defaults({
        /* headers identify fields received */
            headers: _.map(keys, function(key) { 
                return '<header>' + key + '</header>';
            }).join(""),
        /* column elements filled with values from obj received */
            colData: _.map(keys, function(key) {
                return '<column>' + this[key] + '</column>';
            }, fillData).join("")
        }, authData);
};

var integXml = _.template('\
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ws="http://ws.bluemessaging.net"><soapenv:Header/><soapenv:Body>\
<ws:integrate>\
<authentication>\
<token><%= bmToken %></token>\
<domain><%= bmDomain %></domain>\
</authentication>\
<ws:integration>\
    <template><%= bmTemplate %></template>\
    <headers><%= headers %></headers>\
    <data><row><%= colData %></row></data>\
</ws:integration>\
</ws:integrate>\
</soapenv:Body></soapenv:Envelope>');