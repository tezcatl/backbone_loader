var http   = require('http');
var inspect = require('util').inspect;
var bm      = require('./lib/bmHelper');
var _ = require('underscore');
//var url  = 'http://platform.bluemessaging.net/services/bm-api?wsdl';

var args = {
        name : "Prueba2",
        qcredito : 2,
        ejemplo : "yei",
        cVisita : "2",
        nFolio : "2",
        credito : "2",
        comentario : "Hello from node.js"
};

http.createServer(function(req, response) {
    if(req.method !== "POST") {
      response.end('Sorry, nothing but POST requests implemented yet!');
    } else {
    var recArgs='';
    req.on('data', function(chunk) {
        console.log(chunk.toString());
        recArgs += chunk.toString();
    });
    req.on('end', function() {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET");
        response.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Access-Control-Allow-Origin, X-HTTP-Method-Override, Content-Type, Authorization, Accept");
        
        var rqDt;
        try {
            rqDt = JSON.parse(recArgs);
        } catch (e) {
            if(e instanceof SyntaxError) {
            console.log('invalid JSON' + recArgs);
            rqDt = args;
            } else {
                throw e;
            }
        };

bm.makeRequest(rqDt, function(res) {
    //TODO: filter by status code 200||500
    var errCode=0;
    console.log('STATUS: ' + res.statusCode);
    if(res.statusCode >= 500) {
        errCode=[res.statusCode, "Big error:" + response.statusMessage];
    };
    //TODO: add date to response to client
  console.log('Fecha: ' + res.headers.date);
  res.setEncoding('utf8');
  
  res.on('data', function (chunk) {
      if(errCode !== 0) {
          console.log('BODY: ' + chunk);
          response.statusCode = errCode[0];
          response.end(errCode[1]);
      }
  });
  
    var s='';
    res.on('data', function(data) {
        s+=data.toString();
            //console.log(inspect(obj, { colors: true, depth: Infinity }));
    });
    res.on('end', function() {
        if(/<result>ERROR<\/result><cause>/.test(s)) {
            s=s.replace(/.*<cause>([^<]*)<\/cause>.*/, "$1");
            console.log('Error ocurred: ' + s);
            response.statusCode = 401;
            response.end(s);
        } else {
            console.log('Everything was loaded OK!');
            response.statusCode = 200;
            response.end(recArgs);
        }});
    });
    });
    };
}).listen(9000, "");
